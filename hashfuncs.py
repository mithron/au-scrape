import hashlib

# as of http://stackoverflow.com/questions/3431825/generating-a-md5-checksum-of-a-file
def hashfile(afile, hasher, blocksize=65536):
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    return hasher.digest()

def hashlist(fnamelist):
    [(fname, hashfile(io.open(fname, 'rb'), hashlib.sha1())) for fname in fnamelist]