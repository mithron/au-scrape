from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (create_engine, Column, Integer, 
    String, DateTime) 
from creds import *

Base = declarative_base()

class Item(Base):
    __tablename__ = TABLE
 
    un_id = Column(String, primary_key=True)
    name = Column(String)  
    ARTG_id = Column(Integer)
    sponsor = Column(String)
    item_type = Column(String)
    PI = Column(String)
    CMI = Column(String)
    version = Column(Integer)
    createdTimestamp = Column(String, primary_key=True)
    
    def __str__(self):
        
        return str(self.un_id) + str(self.name) + str(self.ARTG_id) + str(self.sponsor) +str(self.item_type) + str(self.PI) + str(self.CMI)
        
    

def init():
    # function for initial PG table creation and so on
    # check if pg is started and set up: sudo service postgresql start
    eng = create_engine('postgresql://'+PG_USER+':'+PG_PASS+'@localhost/'+DBNAME, pool_size=50, max_overflow=100) #, echo=False)
    
    
    Base.metadata.bind = eng        
    Base.metadata.create_all()
    
    os.makedirs('PI', exist_ok=True)
    os.makedirs('CMI', exist_ok=True)
    os.makedirs('Summary', exist_ok=True)
    
    return True