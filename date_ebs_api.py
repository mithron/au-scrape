import requests
import os, io
from datetime import datetime, timedelta
import hashlib
import argparse
from tqdm import tqdm

import dryscraper
from creds import *
from model import Item

# for postgresql abstraction
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy import create_engine


import logging


#get cookies for scraping
cookies = {}

#connection setter
eng = create_engine('postgresql://'+PG_USER+':'+PG_PASS+'@localhost/'+DBNAME, pool_size=50, max_overflow=100) #, echo=False)
sess = scoped_session(sessionmaker(autoflush=True, autocommit=False,bind=eng))


def hashfile(afile, hasher = hashlib.sha1(), blocksize=65536):
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    return hasher.hexdigest()
   
def get_ATRG_summary(un_id):
    resp = requests.get('https://www.ebs.tga.gov.au/servlet/xmlmillr6?dbid=ebs%2FPublicHTML%2FpdfStore.nsf&docid='
                        + str(un_id) + '&agid=(PrintDetailsPublic)&actionid=1', headers={'referer': ref})
    return resp.content
    
    
def get_PICMI_by_id(PICMI_id):
    global cookies
    rqsess = requests.session()
    headers = {"User-Agent":
                "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36"}
    
    resp = rqsess.get('https://www.ebs.tga.gov.au/ebs/picmi/picmirepository.nsf/pdf?OpenAgent&id='+str(PICMI_id), cookies = cookies,
                            headers={'referer': ref})
    if 'text/html' in resp.headers['content-type']:
        cookies = dryscraper.get_cookie()
        resp = rqsess.get('https://www.ebs.tga.gov.au/ebs/picmi/picmirepository.nsf/pdf?OpenAgent&id='+str(PICMI_id), cookies = cookies,
                            headers={'referer': ref})
    return resp.content

def get_sup_info(un_id):
    resp = requests.get('https://www.ebs.tga.gov.au/ebs/home.nsf/getSupInfo?OpenAgent&dbpath=ebs%2FANZTPAR%2FPublicWeb.nsf&dbview=luPortalRegisterPublic&unid='
                        +str(un_id)+'&pt=RP&ut=1', headers={'referer': ref})
    return resp.json()

def save_pdfs(obj):
    for filename in [obj.PI, obj.CMI]:
        if filename:
            filetype = filename.split('-')[2]
            pdf = get_PICMI_by_id(filename)
            with io.open(os.path.join(filetype,str(obj.ARTG_id)+filename+str(obj.createdTimestamp)+'.pdf'), "wb") as pdf_file:
                            pdf_file.write(pdf)
    pdf = get_ATRG_summary(obj.un_id)
    filename = 'Summary'
    with io.open(os.path.join('Summary',str(obj.ARTG_id)+filename+str(obj.createdTimestamp)+'.pdf'), "wb") as pdf_file:
            pdf_file.write(pdf)
    return True

def test_pdfs(obj):
    logging.debug("Testing pdfs in %s" % str(obj))
    for filename in [obj.CMI, obj.PI]:
        if filename:
            filetype = filename.split('-')[2]
            pdf = get_PICMI_by_id(filename)
            localfname = os.path.join(filetype, str(obj.ARTG_id)+filename+str(obj.createdTimestamp)+'.pdf')
            try:
                localhash = hashfile(io.open(localfname, 'rb'),hashlib.sha1())
                remotehash = hashfile(io.BytesIO(pdf),hashlib.sha1())
                if  localhash !=  remotehash:
                    logging.debug('Diff found in %s. its hash is %s and remote is %s' %
                                    (str(localfname), str(localhash), str(remotehash)))
                    return True
            except Exception as exc:
                logging.error('Error occured in tests! %s' % str(exc))
                return True
    ''' For now calculating summary diff is not helping as summary is always generated anew
    pdf = get_ATRG_summary(obj.un_id)
    localfname = os.path.join('Summary',str(obj.ARTG_id)+'Summary'+str(obj.createdTimestamp)+'.pdf')
    try:
        localhash = hashfile(io.open(localfname, 'rb'),hashlib.sha1())
        remotehash = hashfile(io.BytesIO(pdf),hashlib.sha1())
        if localhash != remotehash:
            logging.debug('Diff found in %s.  hash is %s and remote is %s' %
                                    (str(localfname), str(localhash), str(remotehash)))
            return True
    except Exception as exc%:
        logging.error('Error occured in tests! %s' % str(exc))
        return True'''
    logging.debug('PDF dont differ in %s!' % str(obj.ARTG_id))
    return False
            
        
def parse(item):
    a = Item()
    a.un_id = item['@unid']
    logging.debug("UNID is %s" % str(a.un_id))
    for label in item['entrydata']:
        if label['@name'] == 'Name':
            a.name = label['text']['0']
            logging.debug("NAme is %s" % str(a.name))
        elif label['@name'] == 'Sponsor':
            a.sponsor = label['text']['0']
        elif label['@name'] == 'ARTG':
            a.ARTG_id = label['number']['0']
        elif label['@name'] == 'Type':
            a.item_type = label['text']['0']
        elif label['@name'] == 'PI/CMI':
            if 'textlist' in label:
                for entry in label['textlist']['text']:
                    picmi = entry['0'].split(':')
                    logging.debug('PICMI is %s' % str(picmi))
                    filename = picmi[-1]
                    logging.debug("PICMI Label is %s and value %s" % (str(filename), str(picmi[-2])))
                    if picmi[-2] == 'PI':
                        a.PI = filename
                        logging.debug("PI is %s" % str(a.PI))
                    elif picmi[-2] == 'CMI': 
                        a.CMI = filename
                        logging.debug("CMI is %s" % str(a.CMI))
                    else:
                        logging.error('Something different downloaded! %s' % "".join(picmi))
            elif 'text' in label:
                picmi = label['text']['0'].split(':')
                logging.debug('PICMI is %s' % str(picmi))
                if len(picmi) > 1:
                    filename = picmi[-1]
                    logging.debug("PICMI Label is %s and value %s" % (str(filename), str(picmi[-2])))
                    if picmi[-2] == 'PI':
                        a.PI = filename
                        logging.debug("PI is %s" % str(a.PI))
                    elif picmi[-2] == 'CMI': 
                        a.CMI = filename
                        logging.debug("CMI is %s" % str(a.CMI))
                    else:
                        logging.error('Something different downloaded! %s' % "".join(picmi))
        else:
            logging.info("Label not found! %s" % str(label['@name']))
    logging.debug("Item is %s " % str(a))
    return a

def get_by_date(date):
    # date format 25/11/2015
    resp = requests.get('https://www.ebs.tga.gov.au/ebs/home.nsf/SearchViewEntries?OpenAgent&c=PublicWebRecent&q=LicenceStartDate%20%3E%3D%20'+str(date) + '&count=4000&r=0.5527965722139925', 
                                headers={'referer': ref})
    data = resp.json()['viewentry']
    logging.debug("data is %s" % str(data))
    records = []
    for item in data:
        logging.debug("datapart is %s" % str(item))
        records.append(parse(item))
    return records

def main(date = datetime.today() - timedelta(2), loglevel = logging.ERROR):
    
    logging.basicConfig(level = loglevel)
    
    global cookies 
    global sess
    cookies = dryscraper.get_cookie()
    sess = scoped_session(sessionmaker(autoflush=True, autocommit=False,bind=eng))
    db_session = sess()
    if type(date)!= str:
        strdate  = date.strftime('%d/%m/%Y')
    else:
        strdate = date
    try:
        logging.warn('Going for %s' % str(strdate))
        records = get_by_date(str(strdate))
        logging.warn('Got %s from %s' % (str(len(records)), str(strdate)))
        for record in records:
            q = db_session.query(Item).filter(Item.ARTG_id == record.ARTG_id)
            if db_session.query(q.exists()).scalar():
                logging.debug('%s already scraped. Checking diffs.' % str(record.ARTG_id))
                logging.debug("in %s remote is %s" % (str(record.ARTG_id),str(record)))
                q2 = db_session.query(Item).filter(Item.un_id == str(record.un_id)).order_by(Item.version.desc()).first()
                logging.debug('in %s local is %s' % (str(record.ARTG_id),str(q2)))
                if q2:
                    record.createdTimestamp = datetime.now().strftime('%y%m%d_%H%M')
                    if str(record) != str(q2) or test_pdfs(q2):
                        logging.debug('Diff found!')
                        record.version = q2.version + 1
                        save_pdfs(record)
                        db_session.add(record)
                        db_session.commit()
                    else:
                        logging.debug('UNID not found!')
                        record.version = 1
                        record.createdTimestamp = datetime.now().strftime('%y%m%d_%H%M')
                        save_pdfs(record)
                        db_session.add(record)
                        db_session.commit() 
                else:
                    record.version = 1
                    record.createdTimestamp = datetime.now().strftime('%y%m%d_%H%M')
                    save_pdfs(record)
                    db_session.add(record)
                    db_session.commit()
    except Exception as exc:
        logging.error('Error occured in %s! %s' % (str(record.ARTG_id),str(exc)))

    finally:
        db_session.commit()
        logging.warn("Currently %s objects in db" % str(db_session.query(Item.createdTimestamp).count()))
    db_session.close()
    return True

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='Scraper for ARTG DB')
    parser.add_argument('--date', '-d',dest="date", type=str, action='store', default = datetime.today() - timedelta(2),
                   help='License start date, format: dd/mm/yyyy, default two days ago')
    parser.add_argument('--loglevel', dest='loglevel', action='store', type=int, default=40, 
                   help='Logging Level. Default = 40, error. Set 30 for warn and 10 for debug.')

    args = parser.parse_args()
    main(date = datetime.strptime(args.date,"%d/%m/%Y" ), loglevel = args.loglevel )    
