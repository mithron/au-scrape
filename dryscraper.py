import dryscrape


def get_dry_cookies(PICMI_id = 'CP-2011-CMI-01505-3'):
    dryscrape.start_xvfb()
    sess = dryscrape.Session(base_url = 'https://www.ebs.tga.gov.au')
    sess.set_attribute('auto_load_images', False)
    sess.set_header('User-agent', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36')
    sess.visit('/ebs/picmi/picmirepository.nsf/pdf?OpenAgent&id='+PICMI_id)
    agree_link = sess.at_xpath("/html/body/form/div[2]/div[3]/div[1]/a[1]/span")
    agree_link.click()
    return sess.cookies()
    
def get_cookie():
    drycookies = get_dry_cookies()
    rq_cookie = {}
    for line in drycookies:
        if line.find('PICMI') == 0:
            lst = line.split(';')[0].split('=')
            rq_cookie[lst[0]] = lst[1]
    return rq_cookie


