import requests
import os, io
from datetime import datetime 
import hashlib
import argparse
from tqdm import tqdm

import dryscraper

# for postgresql abstraction

from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy import create_engine
    
from threading import Thread, Lock

import logging

from creds import *
from model import Item

#get cookies for scraping

cookies = {}



def hashfile(afile, hasher = hashlib.sha1(), blocksize=65536):
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    return hasher.hexdigest()
   
def get_ATRG_summary(un_id):
    resp = requests.get('https://www.ebs.tga.gov.au/servlet/xmlmillr6?dbid=ebs%2FPublicHTML%2FpdfStore.nsf&docid='
                        + str(un_id) + '&agid=(PrintDetailsPublic)&actionid=1', headers={'referer': ref})
    return resp.content
    
    
def get_PICMI_by_id(PICMI_id):
    global cookies
    rqsess = requests.session()
    headers = {"User-Agent":
                "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36"}
    
    resp = rqsess.get('https://www.ebs.tga.gov.au/ebs/picmi/picmirepository.nsf/pdf?OpenAgent&id='+str(PICMI_id), cookies = cookies,
                            headers={'referer': ref})
    if 'text/html' in resp.headers['content-type']:
        cookies = dryscraper.get_cookie()
        resp = rqsess.get('https://www.ebs.tga.gov.au/ebs/picmi/picmirepository.nsf/pdf?OpenAgent&id='+str(PICMI_id), cookies = cookies,
                            headers={'referer': ref})
    return resp.content


    
def get_sup_info(un_id):
    resp = requests.get('https://www.ebs.tga.gov.au/ebs/home.nsf/getSupInfo?OpenAgent&dbpath=ebs%2FANZTPAR%2FPublicWeb.nsf&dbview=luPortalRegisterPublic&unid='
                        +str(un_id)+'&pt=RP&ut=1', headers={'referer': ref})
    return resp.json()

def save_pdfs(obj):
    for filename in [obj.PI, obj.CMI]:
        if filename:
            filetype = filename.split('-')[2]
            pdf = get_PICMI_by_id(filename)
            with io.open(os.path.join(filetype,str(obj.ARTG_id)+filename+str(obj.createdTimestamp)+'.pdf'), "wb") as pdf_file:
                            pdf_file.write(pdf)
    pdf = get_ATRG_summary(obj.un_id)
    filename = 'Summary'
    with io.open(os.path.join('Summary',str(obj.ARTG_id)+filename+str(obj.createdTimestamp)+'.pdf'), "wb") as pdf_file:
            pdf_file.write(pdf)
    return True

def test_pdfs(obj):
    logging.debug("Testing pdfs in %s" % str(obj))
    for filename in [obj.CMI, obj.PI]:
        if filename:
            filetype = filename.split('-')[2]
            pdf = get_PICMI_by_id(filename)
            localfname = os.path.join(filetype, str(obj.ARTG_id)+filename+str(obj.createdTimestamp)+'.pdf')
            try:
                localhash = hashfile(io.open(localfname, 'rb'),hashlib.sha1())
                remotehash = hashfile(io.BytesIO(pdf),hashlib.sha1())
                if  localhash !=  remotehash:
                    logging.debug('Diff found in %s. its hash is %s and remote is %s' %
                                    (str(localfname), str(localhash), str(remotehash)))
                    return True
            except Exception as exc:
                logging.error('Error occured in tests! %s' % str(exc))
                return True
    ''' For now calculating summary diff is not helping as summary is always generated anew
    pdf = get_ATRG_summary(obj.un_id)
    localfname = os.path.join('Summary',str(obj.ARTG_id)+'Summary'+str(obj.createdTimestamp)+'.pdf')
    try:
        localhash = hashfile(io.open(localfname, 'rb'),hashlib.sha1())
        remotehash = hashfile(io.BytesIO(pdf),hashlib.sha1())
        if localhash != remotehash:
            logging.debug('Diff found in %s.  hash is %s and remote is %s' %
                                    (str(localfname), str(localhash), str(remotehash)))
            return True
    except Exception as exc%:
        logging.error('Error occured in tests! %s' % str(exc))
        return True'''
    logging.debug('PDF dont differ in %s!' % str(obj.ARTG_id))
    return False
            
        
def parse(item):
    a = Item()
    a.un_id = item['@unid']
    logging.debug("UNID is %s" % str(a.un_id))
    for label in item['entrydata']:
        if label['@name'] == 'Name':
            a.name = label['text']['0']
            logging.debug("NAme is %s" % str(a.name))
        elif label['@name'] == 'Sponsor':
            a.sponsor = label['text']['0']
        elif label['@name'] == 'ARTG':
            a.ARTG_id = label['number']['0']
        elif label['@name'] == 'Type':
            a.item_type = label['text']['0']
        elif label['@name'] == 'PI/CMI':
            if 'textlist' in label:
                for entry in label['textlist']['text']:
                    picmi = entry['0'].split(':')
                    logging.debug('PICMI is %s' % str(picmi))
                    filename = picmi[-1]
                    logging.debug("PICMI Label is %s and value %s" % (str(filename), str(picmi[-2])))
                    if picmi[-2] == 'PI':
                        a.PI = filename
                        logging.debug("PI is %s" % str(a.PI))
                    elif picmi[-2] == 'CMI': 
                        a.CMI = filename
                        logging.debug("CMI is %s" % str(a.CMI))
                    else:
                        logging.error('Something different downloaded! %s' % "".join(picmi))
            elif 'text' in label:
                picmi = label['text']['0'].split(':')
                logging.debug('PICMI is %s' % str(picmi))
                if len(picmi) > 1:
                    filename = picmi[-1]
                    logging.debug("PICMI Label is %s and value %s" % (str(filename), str(picmi[-2])))
                    if picmi[-2] == 'PI':
                        a.PI = filename
                        logging.debug("PI is %s" % str(a.PI))
                    elif picmi[-2] == 'CMI': 
                        a.CMI = filename
                        logging.debug("CMI is %s" % str(a.CMI))
                    else:
                        logging.error('Something different downloaded! %s' % "".join(picmi))
        else:
            logging.info("Label not found! %s" % str(label['@name']))
    logging.debug("Item is %s " % str(a))
    return a

def get_by_artg_id(item_id):
    resp = requests.get('https://www.ebs.tga.gov.au/ebs/home.nsf/SearchViewEntries?OpenAgent&c=RegisterPublic&q=%5BLicenceId%3D%5D%3D'
                            + str(item_id) +'&count=4000&r=0.6270714167039841', headers={'referer': ref})
    data = resp.json()['viewentry']
    logging.debug("data is %s" % str(data))
    records = []
    for item in data:
        logging.debug("datapart is %s" % str(item))
        records.append(parse(item))
    return records

def get_by_date(date):
    # date format 25/11/2015
    resp = requests.get('https://www.ebs.tga.gov.au/ebs/home.nsf/SearchViewEntries?OpenAgent&c=PublicWebRecent&q=LicenceStartDate%20%3E%3D%20'+date + '&count=4000&r=0.5527965722139925', 
                                headers={'referer': ref})
    data = resp.json()['viewentry']
    logging.debug("data is %s" % str(data))
    records = []
    for item in data:
        logging.debug("datapart is %s" % str(item))
        records.append(parse(item))
    return records
        
class threadsafe_iter():
    """Takes an iterator/generator and makes it thread-safe by
    serializing call to the `next` method of given iterator/generator.
    """
    def __init__(self, it):
        self.it = it
        self.lock = Lock()

    def __iter__(self):
        return self

    def __next__(self):
        with self.lock:
            return self.it.__next__()

# multithreading

class MTWorker(object):

    def __init__(self, worker_count=5, ARTG_maxid = 270000, start_id = 10000):
        logging.warn('started with wk = %s and is going from %s to %s' % 
                            (str(worker_count), str(start_id), str(ARTG_maxid)))
        self.worker_count = worker_count
        self.it = threadsafe_iter(iter(tqdm(range(start_id, ARTG_maxid))))

# work with selected ARTG_id
    def _worker(self):
        db_session = sess()
        for ARTG_id in self.it:
            try:
                logging.warn('Going for %s' % str(ARTG_id))
                records = get_by_artg_id(str(ARTG_id))
                logging.warn('Got %s from %s' % (str(len(records)), str(ARTG_id)))
                q = db_session.query(Item).filter(Item.ARTG_id == ARTG_id)
                if db_session.query(q.exists()).scalar() and records:
                    logging.debug('%s already scraped. Checking diffs.' % str(ARTG_id))
                    for record in records:
                        logging.debug("in %s remote is %s" % (str(ARTG_id),str(record)))
                        q2 = db_session.query(Item).filter(Item.un_id == str(record.un_id)).order_by(Item.version.desc()).first()
                        logging.debug('in %s local is %s' % (str(ARTG_id),str(q2)))
                        if q2:
                            record.createdTimestamp = datetime.now().strftime('%y%m%d_%H%M')
                            if str(record) != str(q2) or test_pdfs(q2):
                                logging.debug('Diff found!')
                                record.version = q2.version + 1
                                save_pdfs(record)
                                db_session.add(record)
                                db_session.commit()
                        else:
                            logging.debug('UNID not found!')
                            record.version = 1
                            record.createdTimestamp = datetime.now().strftime('%y%m%d_%H%M')
                            save_pdfs(record)
                            db_session.add(record)
                            db_session.commit() 
                else:
                    for record in records:
                        record.version = 1
                        record.createdTimestamp = datetime.now().strftime('%y%m%d_%H%M')
                        save_pdfs(record)
                    db_session.add_all(records)
                db_session.commit()
            except Exception as exc:
                logging.error('Error occured in %s! %s' % (str(ARTG_id),str(exc)))
                continue
            finally:
                db_session.commit()
                logging.warn("Currently %s objects in db" % str(db_session.query(Item.createdTimestamp).count()))
        db_session.close()
        return True

# go through all ARTG_ids
    def start(self):
        try:
            for _i in range(self.worker_count):
                t = Thread(target=self._worker)
                t.start()
        except Exception as exc:
            logging.error('Error occured! %s' % str(exc))
            pass
        finally:
            logging.warn('Batch Fired!')


def main(wk = 10, maxid = 10020, minid = 10000, loglevel = logging.ERROR):
    
    logging.basicConfig(level = loglevel)
    
    global cookies 
    global eng
    global sess
    cookies = dryscraper.get_cookie()
    eng = create_engine('postgresql://'+PG_USER+':'+PG_PASS+'@localhost/'+DBNAME, pool_size=max(2*wk, 20), max_overflow=max(100, 2*wk)) #, echo=False)
    sess = scoped_session(sessionmaker(autoflush=True, autocommit=False,bind=eng))
    mt_worker = MTWorker(worker_count=wk, ARTG_maxid = maxid, start_id = minid)
    mt_worker.start()
        
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='Scraper for ARTG DB')
    parser.add_argument('--workers', '-w',dest="workers", type=int,  action = 'store', default = 20,
                   help='Number of worker processes, default 20')
    parser.add_argument('--max', dest='maxid', action='store', type=int, default=10500, 
                   help='Maximum ARTG ID to go')
    parser.add_argument('--min', dest='minid', action='store', type=int, default=10000, 
                   help='Minimum ARTG ID to start, default 10000')
    parser.add_argument('--loglevel', dest='loglevel', action='store', type=int, default=40, 
                   help='Logging Level. Default = 40, error. Set 30 for warn and 10 for debug.')

    args = parser.parse_args()
    main(wk = args.workers, maxid = args.maxid, minid = args.minid, loglevel = args.loglevel )    
