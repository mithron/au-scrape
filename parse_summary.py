import os
import subprocess
from bs4 import BeautifulSoup as bs
import io
from datetime import datetime 

import argparse

from threading import Thread, Lock

# for postgresql abstraction
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session
from sqlalchemy import (create_engine, Column, Integer, 
    String, DateTime) 
    
from tqdm import tqdm
import logging

# DB Creds
DBNAME = 'test'
PG_USER = 'pguser'
PG_PASS = 'pgpass_local'
#TABLE = 'pages'
TABLE = 'ARTGSummaryTable'
TABLE2 = 'ActiveIngredientsTable'
TABLE3 = 'ComponentsTable'

#connection setter
eng = create_engine('postgresql://'+PG_USER+':'+PG_PASS+'@localhost/'+DBNAME, pool_size=50, max_overflow=100) #, echo=False)
sess = scoped_session(sessionmaker(autoflush=True, autocommit=False,bind=eng))
Base = declarative_base()

# Dict for header-attr connection


class Summary(Base):
    __tablename__ = TABLE
 
    ARTG_id = Column(Integer, primary_key=True)
    Status = Column(String)
    Approval_Area = Column(String)
    Start_Date = Column(String)
    Product_Type = Column(String)
    Effective_date  = Column(String)
    Warnings = Column(String)
    Standard_Indications  = Column(String)
    Specific_indications  = Column(String)
    Additional_product_information = Column(String)
    ContainerType = Column(String)
    ContainerMaterial = Column(String)
    ContainerLifetime = Column(String)
    ContainerTemperature = Column(String)
    ContainerClosure = Column(String)
    ContainerConditions = Column(String)
    versionNumber = Column(Integer, primary_key = True)
    createdTimestamp = Column(String, primary_key=True)
    
    def members(self):
        return [a for a in dir(self) if not a.startswith('_') and a not in ['metadata', 'versionNumber', 'createdTimestamp'] and not callable(getattr(self,a))]
    
    def unassigned_members(self):
        try:
            return [a.replace("_", " ") for a in dir(self) if not a.startswith('_') and
                                        a not in ['metadata', 'createdTimestamp', 'versionNumber'] and
                                        not getattr(self,a) and not callable(getattr(self,a))][0]
        except:
            return False
    
    def __str__(self):
        return "".join([str(getattr(self,memb)) for memb in self.members() if getattr(self,memb)])

    def dict_members(self):
        return [a for a in self.__dict__.keys() if not a.startswith('_')]
        

class Components(Base):
    __tablename__ = TABLE3
    
    Name = Column(String, primary_key=True) 
    DosageForm = Column(String)
    RouteOfAdministration = Column(String)
    VisualIdentification = Column(String)
    ARTG_id = Column(Integer, primary_key=True)
    versionNumber = Column(Integer, primary_key = True)
    createdTimestamp = Column(String, primary_key=True)
    
    def members(self):
        return [a for a in dir(self) if not a.startswith('_') and a not in ['metadata', 'versionNumber', 'createdTimestamp'] and not callable(getattr(self,a))]
        
    def __str__(self):
        return "".join([str(getattr(self,memb)) for memb in self.members() if getattr(self,memb)])
        
class Ingridients(Base):
    __tablename__ = TABLE2
    
    Ingredient_id = Column(Integer, primary_key = True, autoincrement = True)
    Ingredient_Name = Column(String)
    Dosage = Column(String)
    ARTG_id = Column(Integer)
    versionNumber = Column(Integer)
    createdTimestamp = Column(String)

    def members(self):
        return [a for a in dir(self) if not a.startswith('_') and a not in ['metadata', 'versionNumber', 'createdTimestamp', 'Ingredient_id'] and not callable(getattr(self,a))]
    
    def __str__(self):
        return "".join([str(getattr(self,memb)) for memb in self.members() if getattr(self,memb)])

def init():
    # function for initial PG table creation and so on
    # check if pg is started and set up: sudo service postgresql start
    
    Base.metadata.bind = eng
    Base.metadata.drop_all()
    Base.metadata.create_all()
    
    return True


def convert_to_file(pdfpath = os.path.join('Summary','10419Summary151119_1248.pdf')):
    htmlpath = pdfpath.split(".")[0] + '.html'
    hresult = subprocess.check_call(['pdftohtml','-i','-s','-noframes', pdfpath, htmlpath])
    return [htmlpath, hresult]

def convert(pdfpath = os.path.join('Summary','10419Summary151119_1248.pdf')):
    return subprocess.check_output(['pdftohtml','-i','-stdout', pdfpath])
    
def make_soup(html = convert()):
    soup = bs(html,'html.parser')
    body = soup.find('body')
    return body

def make_soup_from_file(htmlpath =  convert_to_file()[0]):
    with io.open(htmlpath, "r") as htmlfile:
        soup = bs(htmlfile,'html.parser')
        body = soup.find('body')
        return body

def make_data(soup = make_soup_from_file()):
    return [item.text for item in soup.findAll('p') if item.text != '\xa0']
    
def get_container(data):
    try:
        index = data.index('Container information')
        return tuple(data[index+7:index+13])
    except:
        logging.error('No container info for ATRG %s' % str(data[2]))
        return tuple([None]*6)

def get_multiline(data, start, stop):
    try:
        begin = data.index(start)
        end = data.index(stop)
        return "".join(data[begin+1:end])
    except:
        logging.error('No %s info for ATRG %s' % (str(start),str(data[2])))
        return None

def get_info(data, info):
    try:
        index = data.index(info)
        return data[index+1]
    except:
        logging.error('No %s info for %s' % ( str(info),str(data[2])))
        return None
    
def get_ingrs(data):
    ingrs = []
    try:
        start = data.index('Active Ingredients') + 1
        try:
            end = data.index('Public Summary') - 1
        except:
            end = len(data) - 1
        logging.debug(end)
        while data[end].count('.') != 1 and data[end].count('mg') != 1 :
                end = end -1
        logging.debug(end)
        while start < end  and len(data) >= start +2 :
            ingridient = Ingridients()
            ingridient.Ingredient_Name = data[start]
            ingridient.Dosage = data[start+1]
            start = start + 2
            ingrs.append(ingridient)
        return ingrs
    except Exception as exc:
        logging.warn('Unxpected stop! %s ' % str(exc))
        return ingrs

def parse_body(data = make_data()):
    summary = Summary()
    summary.ARTG_id = data[2]
    
    (summary.ContainerType, summary.ContainerMaterial, 
        summary.ContainerLifetime, summary.ContainerTemperature, summary.ContainerClosure,
        summary.ContainerConditions) = get_container(data)
        
   # summary.Conditions = get_multiline(data,'Conditions','Products') not needed!
    summary.Status = get_info(data,'Status')
    summary.Approval_Area = get_info(data,'Approval area')
    summary.Start_Date = get_info(data,'ARTG Start Date')
    summary.Product_Type = get_info(data,'Product Type')
    summary.Effective_date  = get_info(data,'Effective date')
    summary.Warnings = get_multiline(data,'Warnings', 'Standard Indications')
    summary.Standard_Indications  = get_multiline(data,'Standard Indications','Specific Indications')
    summary.Specific_indications  = get_multiline(data,'Specific Indications', 'Additional Product information')
    summary.Additional_product_information = get_multiline(data,'Additional Product information', 'Container information' )
    
    comps = []
    ingrs = []
    component_indexes = [i for i,x in enumerate(data) if x == 'Dosage Form']
    for index in component_indexes:
        component = Components()
        component.ARTG_id = data[2]
        component.Name = data[index-1]
        component.DosageForm = get_info(data[index:], 'Dosage Form')
        component.RouteOfAdministration = get_info(data[index:], 'Route of Administration')
        component.VisualIdentification = get_info(data[index:], 'Visual Identification')
        comps.append(component)
        try:
            next_index = component_indexes[component_indexes.index(index)+1]
        except:
            next_index = len(data)
        for ingridient in get_ingrs(data[index:next_index]):
            ingridient.ARTG_id = data[2]
            ingrs.append(ingridient)

        
    return summary, ingrs, comps
    
def convert_all_pdfs(path = 'Summary'):
    arr = []
    for root, dirs, files in tqdm(os.walk(path, topdown=False)):
        for file in files:
            logging.debug('Processing path: %s' % str(file))
            result = convert_to_file(os.path.join(path,file))
            if result[1]:
                logging.error('File not converted: %s' % str(result[0])) 
            else:
                arr.append(make_data(make_soup(result[0])))
    return arr
    
class threadsafe_iter():
    """Takes an iterator/generator and makes it thread-safe by
    serializing call to the `next` method of given iterator/generator.
    """
    def __init__(self, it):
        self.it = it
        self.lock = Lock()

    def __iter__(self):
        return self

    def __next__(self):
        with self.lock:
            return self.it.__next__()

def pdfs(path):
    return [files for root, dirs, files in os.walk(path, topdown=False)]
# multithreading

class MTWorker(object):

    def __init__(self, worker_count=5, path = 'Summary'):
        logging.warn('started with wk = %s and path is %s' % 
                            (str(worker_count), str(path)))
        self.worker_count = worker_count
        self.path = path
        self.it = threadsafe_iter(iter(tqdm(pdfs(path))))

# work with selected ARTG_id
    def _worker(self):
        db_session = sess()
        for direc in self.it:
            for file in direc:
                try:
                    if str(file).split('.')[1] == 'pdf':
                        logging.warn('Going for %s' % str(file))
                        (htmlpath, result) = convert_to_file(os.path.join(self.path,file))
                        summary, ingrs, comps = parse_body(make_data(make_soup_from_file(htmlpath)))
                        os.remove(htmlpath)
                        
                        logging.warn("Got Summary %s" % str(summary))
                        logging.warn("Got Ingridients %s" % str(len(ingrs)))
                        logging.warn("Got Components %s" % str(len(comps)))
                        
                        q = db_session.query(Summary).filter(Summary.ARTG_id == summary.ARTG_id)
                        createdTimestamp = datetime.now().strftime('%y%m%d_%H%M')
                        if db_session.query(q.exists()).scalar():
                            changed = False
                            logging.warn("%s already in db" % str(file))
                            q2 = q.order_by(Summary.versionNumber.desc()).first()
                            
                            if str(summary) != str(q2):
                                changed = True
                                logging.warn('Changed Summary!')
                            for ingridient in ingrs:
                                q3 = db_session.query(Ingridients).filter(Ingridients.ARTG_id == ingridient.ARTG_id,
                                                                        Ingridients.versionNumber == q2.versionNumber).all()
                                not_found_flag = True                                                                        
                                for saved_ingridient in q3:
                                    if str(saved_ingridient) == str(ingridient):
                                        q3.remove(saved_ingridient)
                                        not_found_flag = False
                                if not_found_flag:
                                    changed = True
                                    logging.warn('New ingr!')
                                
                            for component in comps:
                                q4 = db_session.query(Components).filter(Components.ARTG_id == component.ARTG_id,
                                                                        Components.versionNumber == q2.versionNumber).all()
                                not_found_flag = True                                                                        
                                for saved_component in q4:
                                    if str(saved_component) == str(component):
                                        q4.remove(saved_component)
                                        not_found_flag = False
                                if not_found_flag:
                                    changed = True
                                    logging.warn('New Component!')
                                    
                            if q3 or q4:
                                changed = True
                                logging.warn('Removed Ingr or component! %s %s ' "".join(q3))

                            if changed:
                                summary.versionNumber = q2.versionNumber +1
                                summary.createdTimestamp = createdTimestamp
                                db_session.add(summary)
                                db_session.commit()
                                for ingridient in ingrs:
                                    ingridient.versionNumber = q2.versionNumber +1
                                    ingridient.createdTimestamp = createdTimestamp
                                    db_session.add(ingridient)
                                    db_session.commit()
                                for component in comps:
                                    component.versionNumber = q2.versionNumber +1
                                    component.createdTimestamp = createdTimestamp
                                    db_session.add(component)
                                    db_session.commit()
                        else:
                            summary.versionNumber = 1
                            summary.createdTimestamp = createdTimestamp
                            db_session.add(summary)
                            db_session.commit()
                            for ingridient in ingrs:
                                ingridient.versionNumber = 1
                                ingridient.createdTimestamp = createdTimestamp
                                db_session.add(ingridient)
                                db_session.commit()
                            for component in comps:
                                component.versionNumber = 1
                                component.createdTimestamp = createdTimestamp
                                db_session.add(component)
                                db_session.commit()
                        
                except Exception as exc:
                    logging.warn('Error occured in %s! %s' % (str(file),str(exc)))
                    continue
                finally:
                    db_session.commit()
                    logging.warn("Currently %s objects in Summary, %s in Ingridients, %s in Components" %
                                            (str(db_session.query(Summary.createdTimestamp).count()), 
                                            str(db_session.query(Ingridients.createdTimestamp).count()),
                                            str(db_session.query(Components.createdTimestamp).count())))
        db_session.close()
        return True

    def start(self):
        try:
            for _i in range(self.worker_count):
                t = Thread(target=self._worker)
                t.start()
        except Exception as exc:
            logging.error('Error occured! %s' % str(exc))
            pass
        finally:
            logging.warn('Batch Fired!')



def main(wk=5, path = 'Summary', loglevel=logging.ERROR):
    logging.basicConfig(level = loglevel)
    
    mt_worker = MTWorker(worker_count=wk, path = path)
    mt_worker.start()
    
    return True


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='ARTG Summary parser')
    
    parser.add_argument('--workers', '-w',dest="workers", type=int, action='store',  default=20,
                   help='Number of worker processes, default 20')
    parser.add_argument('--path','-p', dest='path', action='store', type=str, default='Summary', 
                   help='Path to Summary PDF directory')
    parser.add_argument('--loglevel', dest='loglevel', action='store', type=int, default=40, 
                   help='Logging Level. Default = 40, error. Set 30 for warn and 10 for debug.')

    args = parser.parse_args()
    main(wk = args.workers, path = args.path, loglevel = args.loglevel)    
